//
//  PageControlView.swift
//  MyStore
//
//  Created by Private on 4/22/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class PageControlView: UIView {
    @IBOutlet weak var pageControl: UIPageControl!
}
