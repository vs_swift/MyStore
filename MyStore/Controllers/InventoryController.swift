//
//  InventoryController.swift
//  MyStore
//
//  Created by Private on 4/22/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

var inventory = ["Rica шампунь для розглаживания и виправления волос","Rica шампунь для розглаживания и виправления волос","Rica шампунь для розглаживания и виправления волос","Rica шампунь для розглаживания и виправления волос","Rica шампунь для розглаживания и виправления волос"]
var inventoryIndex = 0

class InventoryController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inventory.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "inventoryCell") as! InventoryCell
        
        cell.inventoryName?.text = inventory[indexPath.row]
        cell.InventoryPrice?.text = "31.30 EUR"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        inventoryIndex = indexPath.row
    }
}
