//
//  ViewController.swift
//  MyStore
//
//  Created by Private on 4/21/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBAction func userLogin(_ sender: Any) {
        var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if txtUserName.text == "test" && txtPassword.text == "123"{
            
            var centerContainer: MMDrawerController?
            var rootViewController = appDelegate.window!.rootViewController
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            var centerViewControlbundleler = mainStoryboard.instantiateViewController(withIdentifier: "ProductTableViewController") as! ProductTableViewController
            
            var leftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuController") as! MenuController
            
            var leftSideNav = UINavigationController(rootViewController: leftViewController)
            var centerNav = UINavigationController(rootViewController: centerViewControlbundleler)
            
            centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
            
            centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView;
            centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView;
            
            UserDefaults.standard.set(true, forKey: "IsUserLoggedIn");
            UserDefaults.standard.synchronize()
            
            appDelegate.centerContainer = centerContainer
            
            appDelegate.window!.rootViewController = appDelegate.centerContainer
            appDelegate.window!.makeKeyAndVisible()
            
            
             
//            let landingPage = self.storyboard?.instantiateViewController(withIdentifier: "ProductTableViewController") as! ProductTableViewController
//            self.navigationController?.pushViewController(landingPage, animated: true)
        }
        
        
        
    }
     
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

