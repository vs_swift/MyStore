//
//  MenuController.swift
//  MyStore
//
//  Created by Private on 4/21/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class MenuController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var logoImage: UIImageView!

    @IBAction func logOutButton(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "IsUserLoggedIn");
        UserDefaults.standard.synchronize()
        
        var loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! ViewController
        
        var appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = loginViewController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    var menuOptions = ["Каталог товаров","Корзина","Избраное","Мои заказы","Информация","Язикы"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuTableViewCell
        
        cell.itemName.text = menuOptions[indexPath.row]
    
        return cell
    }
}
