//
//  ImagesPageViewController.swift
//  MyStore
//
//  Created by Private on 4/22/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class ImagesPageViewController: UIPageViewController {
    
    var images :[UIImage]? = [UIImage(named:"shampoo1.jpg")!,UIImage(named:"shampoo2.jpg")!, UIImage(named:"shampoo3.jpg")!]
    
    struct Storyboard {
        static let ImageViewController = "ImageViewController"
    }
    lazy var controllers: [UIViewController] = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controllers = [UIViewController]()
        
        if let images = self.images{
            for image in images {
                let imageVC = storyboard.instantiateViewController(withIdentifier:Storyboard.ImageViewController)
                controllers.append(imageVC)
            }
        }
        return controllers
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        self.turnToPage(index: 0)
    }
    
    func turnToPage (index: Int){
        let controller = controllers[index]
        var direction = UIPageViewControllerNavigationDirection.forward
        
        if let currentVC = viewControllers?.first {
            
            let currentIndex = controllers.index(of: currentVC)!
            
            if currentIndex > index {
                direction = .reverse
            }
        }
        self.configureDispaying(viewController: controller)
        setViewControllers([controller], direction: direction, animated: true, completion: nil)
    }
    
    func configureDispaying(viewController: UIViewController) {
        for (index, vc) in controllers.enumerated() {
            if viewController  === vc {
                if let imageVC = viewController as? ImageViewController{
                    imageVC.image = self.images?[index]
                }
            }
        }
    }
}

extension ImagesPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let index = controllers.index(of: viewController) {
            if index > 0 {
                return controllers[index-1]
            }
        }
        return controllers.last
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if  let index = controllers.index(of: viewController) {
             if index < controllers.count - 1{
                return controllers[index+1]
            }
        }
        return controllers.first
    }
}

extension ImagesPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.configureDispaying(viewController: pendingViewControllers.first as! ImageViewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            self.configureDispaying(viewController: previousViewControllers.first as! ImageViewController )
        }
    }
}
