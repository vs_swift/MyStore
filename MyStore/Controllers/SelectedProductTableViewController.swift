//
//  SelectedProductTableViewController.swift
//  MyStore
//
//  Created by Private on 4/21/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

var details = ["Уход за волосами","Наращивание волос","Парики","Украшения для волос"]
var indexNumber = 0

class SelectedProductTableViewController: UITableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SelectedTableViewCell
        cell.SelectedLabel?.text = details[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexNumber = indexPath.row
    }
}
