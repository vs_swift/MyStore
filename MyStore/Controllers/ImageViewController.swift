//
//  ImageViewController.swift
//  MyStore
//
//  Created by Private on 4/22/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            self.imageView? .image = image
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = image
    }
}
