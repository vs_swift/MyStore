//
//  SelectedTableViewCell.swift
//  MyStore
//
//  Created by Private on 4/21/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class SelectedTableViewCell: UITableViewCell {

    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var SelectedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
