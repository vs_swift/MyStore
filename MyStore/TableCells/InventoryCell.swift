//
//  InventoryCell.swift
//  MyStore
//
//  Created by Private on 4/22/18.
//  Copyright © 2018 Vadym Savchuk. All rights reserved.
//

import UIKit

class InventoryCell: UITableViewCell {

    @IBOutlet weak var InventoryImage: UIImageView!
    @IBOutlet weak var inventoryName: UILabel!
    @IBOutlet weak var InventoryPrice: UILabel!
    @IBOutlet weak var ShoppingCart: UIImageView!
    @IBOutlet weak var Favourite: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
